/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabopengl;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

public class Trabopengl implements GLEventListener{

 @Override
 public void display(GLAutoDrawable drawable) {
  
  GL2 gl = drawable.getGL().getGL2();
  
  gl.glClear(GL.GL_COLOR_BUFFER_BIT);
  gl.glColor3f(1f, 0, 0);
  
  
  gl.glBegin(GL.GL_TRIANGLES);
  gl.glVertex2f(70, 50 );
  gl.glVertex2f( 95, 120 );
  gl.glVertex2f( 120, 50 );
  gl.glEnd();
  
  gl.glColor3f(0, 0, 0);
  gl.glBegin(GL2.GL_LINE_LOOP);
  gl.glVertex2f( 40, 40 );
  gl.glVertex2f(40, 130 );
  gl.glVertex2f( 40 , 130  );
  gl.glVertex2f( 150 , 130 );
  gl.glVertex2f( 150 , 130 );
  gl.glVertex2f( 150 , 40 );
  gl.glEnd();
  
 }

 @Override
 public void dispose(GLAutoDrawable arg0) {
  // TODO Auto-generated method stub
  
 }

 @Override
 public void init(GLAutoDrawable drawable) {
  
  GL2 gl = drawable.getGL().getGL2();
  
  gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
  gl.glMatrixMode(GL2.GL_MATRIX_MODE);
  gl.glLoadIdentity();
  
  GLU glu = new GLU();
  glu.gluOrtho2D(0, 200f, 0f, 150f);
  
 }

 @Override
 public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
  // TODO Auto-generated method stub
  
 }
 
 public static void main(String[] args) {
  
 
    GLProfile profile = GLProfile.get(GLProfile.GL2);
    
    GLCapabilities caps = new GLCapabilities(profile);
    
    GLCanvas canvas = new GLCanvas(caps); 
    canvas.addGLEventListener(new Trabopengl()); 
   
   
  
  JFrame frame = new JFrame("Primeiro Programa OPENGL");
  
  frame.add(canvas);
  frame.setSize(400, 300);
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  frame.setVisible(true);
  
 }

}